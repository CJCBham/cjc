<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package CJC
 */

get_header(); ?>

	<div class="row">
		<div class="col-12 col-sm-4 col-md-2 sidebar sidebar-left">
			<div class="section-content">
				<h2 class="section-title" style="font-weight: bold; font-size: 15px; padding-top: 6px;"><a style="color: #AB1E37; text-decoration: none;" href="<?php echo get_site_url(); ?>/media/photos" title="">Go Back</a></h2>
			</div>
		</div>
		<div class="col-12 col-sm-8 col-md-8 content">

			<?php
			while ( have_posts() ) : the_post(); ?>				
				<header class="entry-header">
					<div class="h3 section-title"><?php the_title() ?></div>
				</header><!-- .entry-header -->
				
				<?php 
				the_content();

			endwhile; // End of the loop.
			?>

		</div><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
