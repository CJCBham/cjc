<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package CJC
 * 
 * Template Name: FAQ Donation
 * 
 */

get_header(); ?>
	<div class="row">
		<div class="col-12 col-sm-4 col-md-2 sidebar sidebar-left">
			<div class="section-content">
				<?php echo wpb_list_child_pages(); ?>
			</div>
		</div>
		<div class="col-12 col-sm-8 col-md-8 content">
			<?php
				while ( have_posts() ) : the_post();

                    get_template_part( 'template-parts/content', 'donation' );
                    
                    

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;

				endwhile; // End of the loop.
            ?>
            
            <div class="video-modal modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="videoModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <video id="popup-video" controls src="" autoplay>
                            Your browser does not support the video tag.
                        </video>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                    </div>
                </div>
            </div>

		</div>
	</div>
<?php
get_footer();
