"use strict";

var thePdf = null;
var scale = 1;

function loadPDF(url) {
    PDFJS.getDocument(url).promise.then(function(pdf) {
        thePdf = pdf;
        var viewer = document.getElementById('pdf-viewer');
        viewer.innerHTML = '';
        var canvas = '';
        for(var page = 1; page <= pdf.numPages; page++) {
          canvas = document.createElement("canvas");
          canvas.className = 'pdf-page-canvas';         
          viewer.appendChild(canvas);            
          renderPage(page, canvas);
        }
    });
}

function renderPage(pageNumber, canvas) {
    thePdf.getPage(pageNumber).then(function(page) {
        var viewport = page.getViewport(scale);
        canvas.height = viewport.height;
        canvas.width = viewport.width;
        page.render({canvasContext: canvas.getContext('2d'), viewport: viewport});
    });
}

$(function() {

    // Stop Closing of dropdown close on click
    $('.right-dropdown .dropdown-body').on('click', function(e) {
        e.stopPropagation();
    });

    $('.primary-menu li:nth-child(4) > a').removeClass('dropdown-toggle').removeAttr('data-toggle').removeAttr('aria-haspopup').removeAttr('aria-expanded');

    // $('.nav-link').on({
    //     mouseover: function() {
    //         $(this).next().addClass('show')
    //     },
    //     mouseout: function() {
    //         setTimeout(() => {
                
    //         }, 200);
    //         $(this).next().removeClass('show')
    //     }
    // })

    $('.gallery .gallery-caption').each(function() {
        var data = $(this).text().trim();
        $(this).prev('.gallery-icon').children('a').attr('data-title', data);
        $(this).prev('.gallery-icon').find('img').attr('alt', data);
        $(this).remove();
    });

    // Homepage Menu
    $('.click-toggle').on('click', function() {
        $(this).children('ul.menu-toggle').toggleClass('display-none');
        $(this).toggleClass('background-box-active');
        $(this).toggleClass('click-icon');
    });

    $('.gotop').click(function() {
        $('body,html').animate({
            scrollTop: 0
        }, 400);
        return false;
    });

    $('.footer-search').on({
        focus: function() {
            $(this).closest('.menu-search').addClass('full');
        },
        focusout: function() {
            $(this).closest('.menu-search').removeClass('full');
        }
    });

    $('.pdf-list li').on('click', function() {

        var title = $(this).children('label').html();
        $('.pdf-header').children('h4').html(title);

        // Show PDF Section.

        $('.result-pdf').show();
        
        // remove all active
        $(this).parent('ul').children('li').each(function() {
            $(this).removeClass('active');
        });

        // add active
        $(this).addClass('active');

        var source = $(this).attr('source');
        if (source !== '' || source !== undefined) {
            $('#pdf-link').attr('href', source);
            loadPDF(source);
        } else {
            alert('There seems to be a error loading report. Please try again later.');
        }
        
    });

   /* $('.poster, .video-play').click( function() {
        var video = $(this).closest('.video-block').attr('source');
        $('#popup-video').attr('src', video);      

        // Opening Video Modal
        $('#videoModal').modal('show');
    });
   */

    $('.video-play').click( function(event) {
        var video = $(this).closest('.video-block').attr('source');       
        event.preventDefault();
        event.stopPropagation();
        window.open(video, '_blank');

    });
	
    $('#videoModal').on('hide.bs.modal', function(e) {
        var video = document.getElementById("popup-video");
        video.pause();
    });

    $("#faq-text").keyup(function(){        
        // Retrieve the input field text
        var filter = $(this).val();
        // Loop through the faq list
        $(".faq-group .faq-title").each(function(){
            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                $(this).closest('.faq-group').fadeOut();
            } else {
                $(this).closest('.faq-group').show();
            }
        });
    });

    // Infographics
	
	 $('.list-item').click(function () {
	 $(this).next().toggle();
       
        $(this).toggleClass('active');

	 setTimeout(function () {
            $('img[usemap]').rwdImageMaps();
        });
        if ($('#haidb:visible').length === 1) {
            $("#title-haidb > i").css('transform', 'rotate(180deg)');
        } else {
            $("#title-haidb > i").css('transform', 'none');
        }

        if ($('#wcdmb-A:visible').length === 1) {
            $("#title-wcdmb > i").css('transform', 'rotate(180deg)');
        } else {
            $("#title-wcdmb > i").css('transform', 'none');
        }

        if ($('#tbh:visible').length === 1) {
            $("#title-tbh > i").css('transform', 'rotate(180deg)');
        } else {
            $("#title-tbh > i").css('transform', 'none');
        }

        if ($('#rad:visible').length === 1) {
            $("#title-rad > i").css('transform', 'rotate(180deg)');
        } else {
            $("#title-rad > i").css('transform', 'none');
        }
    });
    $("#haidb-btn-next").click(function () {
        $(".contain-info").css('display', 'none');
        $("#wcdmb").toggle();
        rsRotate();
        if ($('#wcdmb-A:visible').length === 0) {
            scrollToEl('wcdmb-B');
        } else {
            scrollToEl('wcdmb-A');
        }
    });
    $("#wcdmb-A-btn-next").click(function () {
        $(".contain-info").css('display', 'none');
        $("#tbh").toggle();
        rsRotate();
        scrollToEl('tbh');
    });

    $("#wcdmb-A-readMore").click(function () {
        $("#readMore .contain").css('display', 'none');
        $("#contain-wcdmb-A").toggle();
    });

    $(".wcdmb-B-readMore").click(function () {
        $("#readMore .contain").css('display', 'none');
        $("#contain-wcdmb-B-readMore-oa").toggle();
    });

    $(".wcdmb-B-readMore-B").click(function () {
        $("#readMore .contain").css('display', 'none');
        $("#contain-wcdmb-B-readMore-commissioner").toggle();
    });

    $("#wcdmb-B-commissionerForOaths").click(function(){
        $("#readMore .contain").css('display', 'none');
        $("#contain-wcdmb-B-commissioner").toggle();
    });

    $("#tbh-btn-bankruptcyOrder").click(function () {
        $("#readMore .contain").css('display', 'none');
        $("#bankruptcy-order-issue").toggle();
    });

    $("#tbh-btn-debtRepaymentPlan").click(function(){
        $("#readMore .contain").css('display', 'none');
        $("#contain-tbh-debtRepaymentPlan").toggle();
    });

    $("#wcdmb-B-btn-bankruptcyEstate").click(function(){
        $("#readMore .contain").css('display', 'none');
        $("#contain-wcdmb-B-bankruptcy-estate").toggle();
    });

    $("#rad-btn-dischargeAfter7").click(function(){
        $("#readMore .contain").css('display', 'none');
        $("#contain-rad-discharge-after7").toggle();
    });

    $("#rad-btn-discharge5-7Year").click(function(){
        $("#readMore .contain").css('display', 'none');
        $("#contain-rad-discharge-5-7year").toggle();
    });

    $("#tbh-btn-sentForDebt").click(function(){
        $("#readMore .contain").css('display', 'none');
        $("#contain-tbh-sentForDebt").toggle();
    });

    $("#tbh-btn-officialAssignee").click(function(){
        $("#readMore .contain").css('display', 'none');
        $("#contain-tbh-officialAssignee").toggle();
    });

    $("#rad-btn-hdbFlats").click(function(){
        $("#readMore .contain").css('display', 'none');
        $("#contain-rad-hdb-flats").toggle();
    });

    $("#wcdmb-B-btn-next").click(function () {
        $(".contain-info").css('display', 'none');
        $("#tbh").toggle();
        rsRotate();
        scrollToEl('tbh');
    });

    $(".wcdmb-A-btn-show").click(function () {
        $("#wcdmb-A").toggle();
        $("#wcdmb-B").toggle();
        scrollToEl('wcdmb');
    });

    $("#debtRepaymentPlan").click(function(){
        $("#readMore .contain").css('display', 'none');
        $("#contain-tbh-debtRepaymentPlan").toggle();
    });
    $(".wcdmb-B-btn-close").click(function () {
        $("#wcdmb-A").toggle();
        $("#wcdmb-B").toggle();
        scrollToEl('wcdmb');
    });

    $("#tbh-btn-next").click(function () {
        $(".contain-info").css('display', 'none');
        $("#rad").toggle();
        rsRotate();
        scrollToEl('rad');
    });

    function scrollToEl(el) {
        setTimeout(function () {
            $('html, body').animate({
                scrollTop: $("#" + el + "").offset().top
            }, 500);
            el = el.indexOf("-") >= 0 ? el.split("-")[0] : el;
            $("#title-" + el + " > i").css('transform', 'rotate(180deg)');
            $('img[usemap]').rwdImageMaps();
        }, 200);
    }

    function rsRotate() {
        $("#title-haidb > i").css('transform', 'none');
        $("#title-wcdmb > i").css('transform', 'none');
        $("#title-tbh > i").css('transform', 'none');
        $("#title-rad > i").css('transform', 'none');
    }

    jQuery(window).ready(function () {
        $('img[usemap]').rwdImageMaps();
    });
	
	(function (a) {
    a.fn.rwdImageMaps = function () {
        var c = this;
        var b = function () {
            c.each(function () {
                if (typeof(a(this).attr("usemap")) == "undefined") {
                    return
                }
                var e = this, d = a(e);
                a("<img />").on('load', function () {
                    var g = "width", m = "height", n = d.attr(g), j = d.attr(m);
                    if (!n || !j) {
                        var o = new Image();
                        o.src = d.attr("src");
                        if (!n) {
                            n = o.width
                        }
                        if (!j) {
                            j = o.height
                        }
                    }
                    var f = d.width() / 100, k = d.height() / 100, i = d.attr("usemap").replace("#", ""), l = "coords";
                    a('map[name="' + i + '"]').find("area").each(function () {
                        var r = a(this);
                        if (!r.data(l)) {
                            r.data(l, r.attr(l))
                        }
                        var q = r.data(l).split(","), p = new Array(q.length);
                        for (var h = 0; h < p.length; ++h) {
                            if (h % 2 === 0) {
                                p[h] = parseInt(((q[h] / n) * 100) * f)
                            } else {
                                p[h] = parseInt(((q[h] / j) * 100) * k)
                            }
                        }
                        r.attr(l, p.toString())
                    })
                }).attr("src", d.attr("src"))
            })
        };
        a(window).resize(b).trigger("resize");
        return this
    }
})(jQuery);
	
    $("#readMore").on('shown.bs.modal', function () {
        setTimeout(function () {
            $('img[usemap]').rwdImageMaps();
        });
    });
})
