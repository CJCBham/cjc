<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package CJC
 */

?>

<?php
	$featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); 
?>

<?php echo '<div class="col-12 content-md-auto content" style="background-image: url(' .esc_url($featured_img_url). ')">' ?>
	<?php
		the_content();

		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'cjc' ),
			'after'  => '</div>',
		) );
	?>
</div><!-- .entry-content -->

<?php if ( get_edit_post_link() ) : ?>
	
<?php endif; ?>
