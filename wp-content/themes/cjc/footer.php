<?php global $cjcopt; ?>

<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package CJC
 */

?>
		</div>
	</section><!-- #content -->

	<footer class="footer">
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-3 col-md-3">
                    <div class="footer-header gotop">
                        Go to Top
                    </div>
                </div>
				<div class="col-12 col-sm-3 col-md-3">
					<?php dynamic_sidebar( 'footer-links' ); ?>
				</div>
				<div class="col-12 col-sm-3 col-md-3">
					<?php dynamic_sidebar( 'footer-contact' ); ?>
					<div class="mb-sm social-links">
						<div class="display-inline-block mr-sm">
							<a href="<?php echo $cjcopt['cjc-instagram'] ?>" target="_blank"><img src="<?php echo get_template_directory_uri()?>/images/instagram.svg" alt="Instagram"></a>
						</div>
						<div class="display-inline-block">
							<a href="<?php echo $cjcopt['cjc-facebook'] ?>" target="_blank"><img src="<?php echo get_template_directory_uri()?>/images/facebook.svg" alt="Facebook"></a>
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-3 col-md-3">
					<?php dynamic_sidebar( 'footer-newsletter' ); ?>
					<p class="mt-3 ipc">
						Charity/IPC Registration No. <br>
						<?php echo $cjcopt['ipc-no'] ?>
					</p>
				</div>
			</div>
			<div class="row justify-content-end">
                <div class="col-12 col-sm-9 col-md-9">
                    <div class="sub-footer">
                        <div class="row align-items-center">
                            <div class="col-3 col-sm-2 col-md-2 col-lg-2 text-center">
                                <img src="<?php echo $cjcopt['cjc-logo']['url'] ?>" width="60">
                            </div>
                            <div class="col-9 col-sm-10 col-md-10 col-lg-10 footer-end">
							<?php echo $cjcopt['footer-sub'] ?>
                            </div>
                        </div>                    
                    </div>
                </div>
            </div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
