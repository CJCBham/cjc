<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package CJC
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function cjc_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'cjc_body_classes' );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function cjc_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'cjc_pingback_header' );

/* Allow SVG */
function cc_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

function additional_custom_styles() {
	wp_enqueue_style( 'bootstrap',  get_template_directory_uri() .'/css/bootstrap.css', array(), null, 'all' );
	wp_enqueue_style( 'lightboxcss',  get_template_directory_uri() .'/css/lightbox.css', array(), null, 'all' );
	wp_enqueue_style( 'cjcstyles', get_template_directory_uri() . '/css/styles.css' );
	wp_enqueue_script( 'jquery-slim', get_theme_file_uri( '/js/jquery-3.2.1.slim.min.js' ), array(), '1.0', true );
	wp_enqueue_script( 'lightboxjs', get_theme_file_uri( '/js/lightbox-plus-jquery.min.js' ), array(), '1.0', true );
	wp_enqueue_script( 'popper', get_theme_file_uri( '/js/popper.min.js' ), array(), '1.0', true );
	wp_enqueue_script( 'bootstrap', get_theme_file_uri( '/js/bootstrap.min.js' ), array(), '1.0', true );
	wp_enqueue_script( 'pdfjs', get_theme_file_uri( '/js/pdf.js' ), array(), '1.0', true );
//	wp_enqueue_script( 'lightboxjs', get_theme_file_uri( '/js/lightbox-plus-jquery.min.js' ), array(), '1.0', true );
	wp_enqueue_script( 'scripts', get_theme_file_uri( '/js/script.js' ), array(), '1.0', true );
}
add_action( 'wp_enqueue_scripts', 'additional_custom_styles' );

/* Menu List */

// Register Navigation Menus
function custom_navigation_menus() {
	$locations = array(
		'main_menu' => __( 'Header Menu', 'text_domain' ),
		'footer_menu' => __( 'Footer Menu', 'text_domain' ),
	);
	register_nav_menus( $locations );
}

add_action( 'init', 'custom_navigation_menus' );
