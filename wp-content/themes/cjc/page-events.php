<?php
/**
 * The template for displaying all events
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package CJC
 * Template Name: Event List
 */


get_header(); ?>
	<div class="row">
		<div class="col-12 col-sm-4 col-md-2 sidebar sidebar-left">
			<div class="section-content">
				
			</div>
		</div>
		<div class="col-12 col-sm-8 col-md-8 content">

			<div class="h3 section-title"><?php the_title() ?></div>

			<div class="entry-content">
				
				<?php 
					while ( have_posts() ) : the_post();

						the_content(); 
					
					endwhile;
				?>
			</div>

			<?php
				// while ( have_posts() ) : the_post();

				// 	get_template_part( 'template-parts/content', 'page' );

				// 	// If comments are open or we have at least one comment, load up the comment template.
				// 	if ( comments_open() || get_comments_number() ) :
				// 		comments_template();
				// 	endif;

				// endwhile; // End of the loop.
			?>

			<?php $loop = new WP_Query( array( 'post_type' => 'events', 'posts_per_page' => 10 ) ); ?>

			<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

				<?php the_title( '<h2 class="entry-title"><a href="' . get_permalink() . '" title="' . the_title_attribute( 'echo=0' ) . '" rel="bookmark">', '</a></h2>' ); ?>

				<div class="entry-content">
					<?php the_content(); ?>
				</div>
			<?php endwhile; ?>
		</div>
	</div>
<?php
get_footer();
