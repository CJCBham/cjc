<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package CJC
 * 
 * Template Name: Self Help Home
 * 
 */

get_header('services'); ?>
	<?php while ( have_posts() ) : the_post(); ?>

	<div class="row">
		<div class="col-12 side-md-20 sidebar-left">
			<div class="section-content">
				<h2 class="section-title"><?php the_title() ?></h2>
			</div>
		</div>

		<?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); ?>

		<?php echo '<div class="col-12 content-md-auto self-help-home content" style="background-image: url(' .esc_url($featured_img_url). ')">' ?>
			<div class="group-page-link row">				
				<?php
					the_content();
					wp_link_pages( array(
						'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'cjc' ),
						'after'  => '</div>',
					));
				?>
			</div>
		<?php echo '</div>'; ?>

		<div class="col-12 side-md-20 sidebar-right">
			<?php
				$menu_name = 'help-sidebar';
				$locations = get_nav_menu_locations();
				$menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
				$menuitems = wp_get_nav_menu_items( $menu->term_id, array( 'order' => 'DESC' ) );
			?>
		<!--	<ul class="list-help">
				<li>
					<div class="dropdown-sidebar dropdown">
						<a href="#" class="text-link">Need help?</a><a class="dropdown-toggle" href="#" id="dropdown-sidebar02" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></a>
						<ul class="dropdown-menu" aria-labelledby="dropdown-sidebar02">-->

							<?php
								$count = 0;
								$submenu = false;
								foreach( $menuitems as $item ):
									$link = $item->url;
									$title = $item->title;
									// item does not have a parent so menu_item_parent equals 0 (false)
									if ( !$item->menu_item_parent ):
									// save this id for later comparison with sub-menu items
									$parent_id = $item->ID;
								?>

								<li>
									<a href="<?php echo $link; ?>">
										<?php echo $title; ?>
									</a>
								<?php endif; ?>
								
								</li>

							<?php $count++; endforeach; ?>

							<!-- <li><a class="dropdown-item" href="#">需要帮忙</a></li>
							<li><a class="dropdown-item" href="#">Perlu Bantuan?</a></li>
							<li><a class="dropdown-item" href="#">உதவி தேவை</a></li> -->
						<!--</ul> 
					</div>
				</li>
			</ul>-->
		</div>		
	</div>
<?php
endwhile; // End of the loop.
get_footer();
