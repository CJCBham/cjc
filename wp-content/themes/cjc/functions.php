<?php
/**
 * CJC functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package CJC
 */


if ( ! function_exists( 'cjc_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function cjc_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on CJC, use a find and replace
		 * to change 'cjc' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'cjc', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-primary' => esc_html__( 'Primary', 'cjc' ),
			'menu-homepage' => esc_html__( 'Homepage Bottom', 'cjc' ),
			'menu-right-homepage' => esc_html__( 'Homepage Right Menu', 'cjc' ),
			'menu-services' => esc_html__( 'Services', 'cjc' ),
			'help-sidebar' => esc_html__( 'Self Help Sidebar', 'cjc' ),
			'help-left' => esc_html__( 'Self Help Left Menu', 'cjc' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'cjc_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'cjc_setup' );


/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function cjc_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'cjc_content_width', 640 );
}
add_action( 'after_setup_theme', 'cjc_content_width', 0 );

if (!class_exists('ReduxFramework')) {
	require_once (dirname(__FILE__) . '/inc/ReduxCore/framework.php');
}

if (!isset($redux_opt)) {
	require_once (dirname(__FILE__) . '/inc/og-config.php');
}

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function cjc_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'cjc' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'cjc' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Links', 'cjc' ),
		'id'            => 'footer-links',
		'description'   => esc_html__( 'Add widgets here.', 'cjc' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));

	register_sidebar( array(
		'name'          => esc_html__( 'Contact Footer', 'cjc' ),
		'id'            => 'footer-contact',
		'description'   => esc_html__( 'Add widgets here.', 'cjc' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));

	register_sidebar( array(
		'name'          => esc_html__( 'Newsletter Footer', 'cjc' ),
		'id'            => 'footer-newsletter',
		'description'   => esc_html__( 'Add widgets here.', 'cjc' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));

}
add_action( 'widgets_init', 'cjc_widgets_init' );


// Gallery Support

// Register Custom Post Type
function custom_post_type() {

	$labels = array(
		'name'                  => _x( 'Galleries', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Gallery', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Gallery', 'text_domain' ),
		'name_admin_bar'        => __( 'Post Type', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Edit Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'View Item', 'text_domain' ),
		'view_items'            => __( 'View Items', 'text_domain' ),
		'search_items'          => __( 'Search Item', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Gallery', 'text_domain' ),
		'description'           => __( 'List of all Galleries', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies'            => array( 'category', 'post_tag', 'page-category' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'gallery', $args );

}
add_action( 'init', 'custom_post_type', 0 );


/**
 * Enqueue scripts and styles.
 */
function cjc_scripts() {
	wp_enqueue_style( 'cjc-style', get_stylesheet_uri() );

	wp_enqueue_script( 'cjc-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'cjc-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'cjc_scripts' );

function add_taxonomies_to_pages() {
	register_taxonomy_for_object_type( 'post_tag', 'page' );
	register_taxonomy_for_object_type( 'category', 'page' );
	}
   add_action( 'init', 'add_taxonomies_to_pages' );
	if ( ! is_admin() ) {
	add_action( 'pre_get_posts', 'category_and_tag_archives' );
	
	}
   function category_and_tag_archives( $wp_query ) {
   $my_post_array = array('post','page');
	
	if ( $wp_query->get( 'category_name' ) || $wp_query->get( 'cat' ) )
	$wp_query->set( 'post_type', $my_post_array );
	
	if ( $wp_query->get( 'tag' ) )
	$wp_query->set( 'post_type', $my_post_array );
}

function wpb_list_child_pages() { 
	
   global $post; 
	
   if ( is_page() && $post->post_parent )	
	   $childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->post_parent . '&echo=0' );
   else
	   $childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->ID . '&echo=0' );	
   if ( $childpages ) {	
	   $string = '<ul class="list-unstyled page-list">' . $childpages . '</ul>';
   } else {
	   $string = '';
   }

   return $string;

}

add_shortcode('wpb_childpages', 'wpb_list_child_pages');


function addRow($attr, $content = null) {
    return '<div class="row">'.$content.'</div>';
}

add_shortcode( 'row', 'addRow' );


// team and members

function addTeam($attr, $content = null) {	

    return '<div class="row">'.do_shortcode($content).'</div>';
}

add_shortcode( 'team', 'addTeam' );


function addMember($attr, $content = null) {
	
	$att = shortcode_atts( array(
        'source' => 'http://placehold.it/80x80'
	), $attr );

	return '<div class="col-12 col-sm-12 col-md-6">
		<div class="member">
			<div class="row align-items-center">
				<div class="col-12 col-sm-6 col-md-3">
					<figure>
						<img src="' . esc_attr($att['source']) . '" class="img-rounded" />
					</figure>
				</div>
				<div class="col-12 col-sm-6 col-md-9 team-desc">
					'.$content.'
				</div>
			</div>
		</div>
	</div>';
}

add_shortcode( 'member', 'addMember' );


// Financial Report

function addReport($attr = [], $content = null) {	
	return 
	'<div class="row">
		<div class="col-12">
			<ul class="pdf-list">'.do_shortcode($content).'</ul>
		</div>
	</div>
	
	<div class="row">
		<div class="col-12">
			<div class="result-pdf mt-4 mb-4">
				<div class="pdf-header p-3">
					<h4 class="display-inline-block mb-0">Result: Bankruptcy Form</h4>
					<div class="float-right">
						<div class="display-inline-block pr-2"><small>Expand</small>
					</div>
					<div class="display-inline-block">
						<a id="pdf-link" href="" target="_blank"><img src="' . get_template_directory_uri() . '/images/expand.svg" width="25px"></a>
					</div>
					</div>
				</div>
				<div class="pdf-content text-center">
					<div id="pdf-viewer"></div>
				</div>
			</div>
		</div>
	</div>
	';
}

add_shortcode( 'report', 'addReport' );

function addReportList($attr = [], $c = null) {

	$att = shortcode_atts( array(
		'pdf' => '',
		'img' => 'http://placehold.it/150x220',
		'content' => ''
	), $attr );

	return 
	'<li source="' . esc_attr($att['pdf']) . '">
		<img src="' . esc_attr($att['img']) . '" />
		<label>' . esc_attr($att['content']) . '</label>
	</li>';
}

add_shortcode( 'list', 'addReportList' );

// Video Shortcode...

function addVideo($attr = [], $content = null) {

	$att = shortcode_atts( array(
		'poster' => 'http://placehold.it/960x400&text=.',
		'source' => '',
		'title' => 'Video Title',
		'description' => 'Some description about the video.'
	), $attr );

	return 
	'<div class="video-block" source="' . esc_attr($att['source']) . '">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-8">
				<div class="poster" style="background-image: url(' . esc_attr($att['poster']) . ');"></div>
			</div>
			<div class="col-12 col-sm-12 col-md-4">
				<div class="video-title">' . esc_attr($att['title']) . '</div>
				<div class="video-desc">
					' . esc_attr($att['description']) . '
				</div>
				<div class="video-play">Watch Now</div>
			</div>
		</div>
	</div>';
}

add_shortcode( 'embedvideo', 'addVideo' );

// Article Shortcode

function addArticle($attr = [], $content = null) {

	$att = shortcode_atts( array(
		'poster' => 'http://placehold.it/960x400&text=.',
		'link' => '',
		'title' => 'Video Title',
		'description' => 'Some description about the video.',
		'content' => 'View Link'
	), $attr );

	return 
	'<div class="article-block">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-8">
				<a target="_blank" href="' . esc_attr($att['link']) .  '">
					<div class="poster" style="background-image: url(' . esc_attr($att['poster']) . ');"></div>
				</a>
			</div>
			<div class="col-12 col-sm-12 col-md-4">
				<div class="article-title">' . esc_attr($att['title']) . '</div>
				<div class="article-desc">
					' . esc_attr($att['description']) . '
				</div>
				<div class="article-play"><a target="_blank" href="' . esc_attr($att['link']) .  '">' . esc_attr($att['content']) . '</a></div>
			</div>
		</div>
	</div>';
}

add_shortcode( 'article', 'addArticle' );

// FAQ Search

function addFaqSearch() {

	return 
	'<div class="input-group faq-search">
		<span class="input-group-addon" id="search-faq"></span>
		<input type="text" id="faq-text" class="form-control" placeholder="Search Questions" aria-label="search-faq" aria-describedby="search-faq">
	</div>';
}

add_shortcode( 'faq-search', 'addFaqSearch' );

// FAQ Group and List

function addFaqGroup($attr = [], $content = null) {	

	$att = shortcode_atts( array(
        'title' => 'FAQ'
	), $attr );

	return 
	'
	<div class="row">
		<div class="col-12">
			<h3 class="faq-header">' . esc_attr($att['title']) . '</h3>
		</div>
	</div>
	<div class="faq-section">
		' .do_shortcode($content). '
	</div>';
}

add_shortcode( 'faq-group', 'addFaqGroup' );


function addFAQ($attr, $content = null) {
	
	$att = shortcode_atts( array(
		'title' => 'Your Title Here..',
		'link' => ''
	), $attr );

/*	return 
	'<div class="row align-items-center faq-group">
		<div class="col-12 col-sm-12 col-md-9">
			<div class="faq-title">' . esc_attr($att['title']) . '</div>
			<div class="answer">
				' . $content . '
			</div>
		</div>
		<div class="col-12 col-sm-12 col-md-3">
			<a href="' . esc_attr($att['link']) . '" class="btn btn-sm btn-red">Bring me to link</a>
		</div>
	</div>';*/


	 return
        '<div class="row align-items-center faq-group">
                <div class="col-12 col-sm-12 col-md-9">
                        <div class="faq-title">' . esc_attr($att['title']) . '</div>
                        <div class="answer">
                                ' . $content . '
                        </div>
                </div>
        </div>';

}

add_shortcode( 'faq', 'addFAQ' );

function clearButton($attr, $content = null) {

	$att = shortcode_atts( array(
		'link' => '#',
	), $attr );

    return '<a href="' . esc_attr($att['link']) . '" class="btn btn-block btn-ico btn-clear">'.$content.'</a>';
}

add_shortcode( 'btn-clear', 'clearButton' );
 
function serviceIntro($attr, $content = null) {
	return '<p class="service-intro">'.$content.'</p>';
}

add_shortcode( 'service-desc', 'serviceIntro' );


// Services Main

function sort_objects_by_total($a, $b) {
	if($a->ID == $b->ID){ return 0 ; }
	return ($a->ID < $b->ID) ? -1 : 1;
}

function servicesAdd($attr = [], $content = null) {	
	$att = shortcode_atts( array(
		'service0' => '',
		'service1' => '',
		'service2' => '',
		'service3' => ''
		
	), $attr );
	

	$menu_name = 'menu-services';
	$locations = get_nav_menu_locations();
	$menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
	$menuitems = wp_get_nav_menu_items($menu->term_id, array( 'order' => 'DESC' ));
	//$menuitems = wp_get_nav_menu_items($menu->term_id, array( 'order' => 'ASC' ));
	$count = 0;
	$parentid = -1;
	$submenu = false;
	$menu_counter=0;
	$content = '';
	$dropdownCounter = 1;


//usort($menuitems, function($a, $b){return $a['ID'] - $b['ID'];});
//usort($menuitems, 'sort_objects_by_total');
//echo json_encode($menuitems);



	
	$content .= '<ul class="row group-service-link">';

	foreach( $menuitems as $item ){
		
		// echo $item->title."<br>";
        $link = $item->url;
		$title = $item->title;
		if($item->menu_item_parent==0){
			$title_match='1';
			$parentid=$item->ID;
			$print=true;
		}
		if($print==true ||  $item->menu_item_parent==$parentid):
        // item does not have a parent so menu_item_parent equals 0 (false)
        if ( !$item->menu_item_parent):
        // save this id for later comparison with sub-menu items
		$parent_id = $item->ID;
		// echo $print;
		
    ?>	

	<?php    
		$content .= '<li class="col-6 col-xl-3">
			<div class="service-dropdown">
				<a class="dropdown-toggle" href="#" id="service-dropdown' .$dropdownCounter. '" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><strong>'.substr($title,0,1).'</strong>'.substr($title,1,strlen($title)-1). '</a>';
	?>
    <?php  endif; ?>		


        <?php if ( $parent_id == $item->menu_item_parent ){ ?>
            <?php if ( !$submenu ): $submenu = true;  //$dropdownCounter--; ?>
            <?php $content .= '<ul class="dropdown-menu" aria-labelledby="service-dropdown' .$dropdownCounter. '">'; ?>
            <?php endif; ?>

				<?php
					$content .= '<li>
						<a href="'. $link .'" class="dropdown-item">' .$title. '</a>
					</li>';
				?>

            <?php  if($count<= (sizeof($menuitems) - 1)){
						if($count!=sizeof($menuitems) - 1){
							if ( $menuitems[ $count + 1]->menu_item_parent != $parent_id && $submenu){ ?>
			
           						 <?php $content .= '</ul>'; ?>
                   				 <?php ?>
			
								<?php $submenu = false; }
						} 
						else if(sizeof($menuitems) - 1==$count){?>
 								 <?php $content .= '</ul>'; ?>
                   				 <?php  ?>			
								<?php $submenu = false;	
						}
						else{

						}
					} else { }
			}?>

    <?php  if($count<= (sizeof($menuitems) - 1)){
			if($count!=sizeof($menuitems) - 1){
				 if ( $menuitems[ $count + 1]->menu_item_parent != $parent_id ){ ?>
   					 <?php $content .= '</div>'; ?>
					<?php $content .= '<p class="desc">'; ?>
						<?php $content = $content. esc_attr($att['service'.$menu_counter]); ?>
					<?php $content .= '</p></li>'; ?>
					 <?php $submenu = false; 
					 $menu_counter++; $dropdownCounter++;
				 }
		 	}
			else if(sizeof($menuitems) - 1==$count){
				$content .= '</div>';
				$content .= '<p class="desc">';
				$content = $content. esc_attr($att['service'.$menu_counter]); 
				$content .= '</p></li>';
				$menu_counter++; $dropdownCounter++;
			        $submenu = false;
			}
			else{

			}

		}else{}?>
		<?php endif;?>
	<?php  $count++; } ?>
		
	<?php
	
	$content .= "</ul>";

    return $content;
}

add_shortcode( 'services', 'servicesAdd' );

// Self help links

function addListLinks($attr = [], $content = null) {

	return 
	'<div class="list-form">
		<ul>
			' .do_shortcode($content). '
		</ul>
	</div>';
}

add_shortcode( 'link-list', 'addListLinks' );


function addLinks($attr, $content = null) {
	
	$att = shortcode_atts( array(
		'url' => '#'
	), $attr );

	return 
	'
	<li>
		<a href="' . esc_attr($att['url']) . '">'. $content .'</a>
	</li>';
}

add_shortcode( 'link', 'addLinks' );

function addInfoBox($attr = [], $content = null) {

	return 
	'
	<div class="document-desc">
		<div class="wp-editor">
			' .$content. '
		</div>
	</div>';
}

add_shortcode( 'infobox', 'addInfoBox' );

function addInfoButton($attr = [], $content = null) {

	$att = shortcode_atts( array(
		'url' => '#',
	), $attr );

	return 
	'
	<div class="action-button">
		<a href="'. esc_attr($att['url']) .'" class="btn-gray">' .$content. '</a>
	</div>';
}

add_shortcode( 'infobutton', 'addInfoButton' );

function addSelfHelp($attr, $content = null) {

	$att = shortcode_atts( array(
		'title' => 'Title Here',
		'link'	=> '#'
	), $attr );

	return '
	<a href="'. esc_attr($att['link']) .'" class="col-12 col-sm-4">
		<span class="title">'. esc_attr($att['title']) .'</span>
		<span class="desc">'. $content .'</span>
	</a>';
}

add_shortcode( 'self-help', 'addSelfHelp' );

// Gallery

//deactivate WordPress function
remove_shortcode('gallery', 'gallery_shortcode');
//activate own function
add_shortcode('gallery', 'msdva_gallery_shortcode'); 
 
function msdva_gallery_shortcode($attr) {
	$post = get_post();
	static $instance = 0;
	$instance++;
	if ( ! empty( $attr['ids'] ) ) {
		// 'ids' is explicitly ordered, unless you specify otherwise.
		if ( empty( $attr['orderby'] ) )
			$attr['orderby'] = 'post__in';
		$attr['include'] = $attr['ids'];
	}
	// Allow plugins/themes to override the default gallery template.
	$output = apply_filters('post_gallery', '', $attr);
	if ( $output != '' )
		return $output;
	// We're trusting author input, so let's at least make sure it looks like a valid orderby statement
	if ( isset( $attr['orderby'] ) ) {
		$attr['orderby'] = sanitize_sql_orderby( $attr['orderby'] );
		if ( !$attr['orderby'] )
			unset( $attr['orderby'] );
	}
	extract(shortcode_atts(array(
		'order'      => 'ASC',
		'orderby'    => 'menu_order ID',
		'id'         => $post ? $post->ID : 0,
		'itemtag'    => 'figure',
		'icontag'    => 'div',
		'captiontag' => 'dd',
		'columns'    => 3,
		'size'       => 'thumbnail',
		'include'    => '',
		'exclude'    => '',
		'link'       => 'file' // CHANGE #1
	), $attr, 'gallery'));
	$id = intval($id);
	if ( 'RAND' == $order )
		$orderby = 'none';
	if ( !empty($include) ) {
		$_attachments = get_posts( array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
		$attachments = array();
		foreach ( $_attachments as $key => $val ) {
			$attachments[$val->ID] = $_attachments[$key];
		}
	} elseif ( !empty($exclude) ) {
		$attachments = get_children( array('post_parent' => $id, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
	} else {
		$attachments = get_children( array('post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
	}
	if ( empty($attachments) )
		return '';
	if ( is_feed() ) {
		$output = "\n";
		foreach ( $attachments as $att_id => $attachment )
			$output .= msdva_wp_get_attachment_link($att_id, $size, true) . "\n";
		return $output;
	}
	$itemtag = tag_escape($itemtag);
	$captiontag = tag_escape($captiontag);
	$icontag = tag_escape($icontag);
	$valid_tags = wp_kses_allowed_html( 'post' );
	if ( ! isset( $valid_tags[ $itemtag ] ) )
		$itemtag = 'dl';
	if ( ! isset( $valid_tags[ $captiontag ] ) )
		$captiontag = 'dd';
	if ( ! isset( $valid_tags[ $icontag ] ) )
		$icontag = 'dt';
	$columns = intval($columns);
	$itemwidth = $columns > 0 ? floor(100/$columns) : 100;
	$float = is_rtl() ? 'right' : 'left';
	$selector = "gallery-{$instance}";
	$gallery_style = $gallery_div = '';
	
	$size_class = sanitize_html_class( $size );
	$gallery_div = "<div id='$selector' class='gallery galleryid-{$id} gallery-columns-{$columns} gallery-size-{$size_class}'>";
	$output = apply_filters( 'gallery_style', $gallery_style . "\n\t\t" . $gallery_div );
		// NOTE: 
		// wp_get_attachment_link = 
		// takes ($id = 0, $size = 'thumbnail', $permalink = false, $icon = false, $text = false)
	$i = 0;
	
	
	foreach ( $attachments as $id => $attachment ) {
		$image_output = msdva_wp_get_attachment_link( $id, $size, true, false );
	
		$image_meta  = wp_get_attachment_metadata( $id );
		$orientation = '';
		if ( isset( $image_meta['height'], $image_meta['width'] ) )
			$orientation = ( $image_meta['height'] > $image_meta['width'] ) ? 'portrait' : 'landscape';
		$output .= "<{$itemtag} class='gallery-item'>";
		$output .= "
			<{$icontag} class='gallery-icon {$orientation}'>
				$image_output
			</{$icontag}>";
		if ( $captiontag && trim($attachment->post_excerpt) ) {
			$output .= "
				<{$captiontag} class='wp-caption-text gallery-caption'>
				" . wptexturize($attachment->post_excerpt) . "
				</{$captiontag}>";
		}
		$output .= "</{$itemtag}>";
	}
	$output .= "
		</div>\n";
	return $output;
}
function msdva_wp_get_attachment_link( $id = 0, $size = 'thumbnail', $permalink = true, $icon = false, $text = false ) {
	$id = intval( $id );
	$_post = get_post( $id );
	
	if ( empty( $_post ) || ( 'attachment' != $_post->post_type ) || ! $url = wp_get_attachment_url( $_post->ID ) )
		return __( 'Missing Attachment' );
	if ( $permalink )
		// $url = get_attachment_link( $_post->ID ); // we want the "large" version!!
		// FIX!! ask for large URL
		$image_attributes = wp_get_attachment_image_src( $_post->ID, 'large' );
		$url = $image_attributes[0];
//		$url = wp_get_attachment_image( $_post->ID, 'large' );
	$post_title = esc_attr( $_post->post_title );
	if ( $text )
		$link_text = $text;
	elseif ( $size && 'none' != $size )
		$link_text = wp_get_attachment_image( $id, $size, $icon );
	else
		$link_text = '';
	if ( trim( $link_text ) == '' )
		$link_text = $_post->post_title;
	return apply_filters( 'wp_get_attachment_link', "<a href='$url' data-lightbox='gallery' rel='gallery-nr'>$link_text</a>", $id, $size, $permalink, $icon, $text );
}

/* 
	List of all Shortcodes...
*/
require get_template_directory() . '/inc/custom-shortcode.php';

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}
function jp_search_filter( $query ) {
  if ( ! $query->is_admin && $query->is_search && $query->is_main_query() ) {
    $query->set( 'post__not_in', array( 251 ) );
  }
}
add_action( 'pre_get_posts', 'jp_search_filter' );
