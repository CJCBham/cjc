<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package CJC
 * 
 * Template Name: Services
 * 
 */

get_header('services'); ?>
	<div class="row">
		<div class="col-12 sidebar side-md-20 sidebar-left">
			<div class="section-content">
				<h2 class="section-title"><?php the_title() ?></h2>
			</div>
		</div>
		<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'services' );
				
				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
		?>
	</div>
<?php
get_footer();