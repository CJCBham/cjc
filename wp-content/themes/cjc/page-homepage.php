<?php global $cjcopt; ?>

<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package CJC
 * Template Name: Homepage
 */
?>
<head>
	<meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width,&#32;initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<?php wp_head(); ?>
	<link href="<?php echo get_template_directory_uri()?>/css/home.css" rel="stylesheet" />
</head>
<body>
	<div class="container" style="max-width: none;">
		<div class="row" style="background-image: url(<?php echo get_template_directory_uri()?>/images/homepage.jpg);background-color: gray;background-repeat: no-repeat;background-size: cover;height: 100vh;min-height: 550px;position: relative">
			<div class="col-md-10 layer">
				<div class="content-home">
					<div class="row" style="padding: 5% 2% 20% 10%;">
						<a href="<?php echo get_site_url(); ?>">
							<img src="<?php echo $cjcopt['cjc-logo']['url'] ?>" width="100" height="100">
						</a>
					</div>
					<div class="row" style="padding: 0% 2% 0% 10%;margin-top: -50px;">
						<div class="text-homepage-cjc" style="font-size:1.6rem !important;color:#d8d2d1;">
							<?php while ( have_posts() ) : the_post(); 
							
								the_content();
							
							endwhile; // End of the loop. ?>
						</div>
						<span class="arrow-right-border learn-more">
							<a href="http://cjc.org.sg/about/">Learn more</a>
							<span class="glyphicons glyphicons-circle-arrow-right"></span>
						</span>
					</div>
				</div>

				<?php
					$menu_name = 'menu-homepage';                        
					$locations = get_nav_menu_locations();
					$menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
					$menuitems = wp_get_nav_menu_items( $menu->term_id, array( 'order' => 'DESC' ) );
					
				?>
				<div class="row boder-white">
					<?php
						$count = 0;
						$submenu = false;
						foreach( $menuitems as $item ):
							$link = $item->url;
							$title = $item->title;
							// item does not have a parent so menu_item_parent equals 0 (false)
							if ( !$item->menu_item_parent ):
							// save this id for later comparison with sub-menu items
							$parent_id = $item->ID;
						?>
						<div class="col-md-3 btn-nav-footer boder-r-white align-center">
							<a href="<?php echo $link; ?>" class="ft-h-cjc"><?php echo $title; ?></a>
						<?php endif; ?>
						<?php if($count< (sizeof($menuitems) - 1)) { if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id ): ?>
						</div>
						<?php $submenu = false; endif; } ?>
					<?php $count++; endforeach;  ?>
				</div>
			</div>
			</div>
			<div class="col-md-2 n-padding">
				
				<?php
					$menu_name = 'menu-right-homepage';
					$locations = get_nav_menu_locations();
					$menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
					$menuitems = wp_get_nav_menu_items( $menu->term_id, array( 'order' => 'DESC' ) );
				?>

				<?php
				$count = 0;
				$submenu = false;
				foreach( $menuitems as $item ):
					$link = $item->url;
					$title = $item->title;
					// item does not have a parent so menu_item_parent equals 0 (false)
					if ( !$item->menu_item_parent ):
					// save this id for later comparison with sub-menu items
					$parent_id = $item->ID;
				?>
				<div id="find-help-box" class="click-toggle click-icon col-md-12 col-sm-12 question-icon n-padding">
					<p class="title goto-bottom c-white p-t-15 f-s-large n-margin"><?php echo $title; ?></p>
				<?php endif; ?>
					<?php if ( $parent_id == $item->menu_item_parent ): ?>
					<?php if ( !$submenu ): $submenu = true; ?>
					<ul class="menu-toggle display-none n-padding">
						<?php endif; ?>
						<li class="arrow-right-border-sb">
							<a href="<?php echo $link; ?>" class="title"><?php echo $title; ?></a>
						</li>
						<?php if($count< (sizeof($menuitems) - 1)) { if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id && $submenu ): ?>
						</ul>
						<?php $submenu = false; endif; } ?>
					<?php endif; ?>
				<?php if($count< (sizeof($menuitems) - 1)) { if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id ): ?>
				</div>
				<?php $submenu = false; endif; } ?>
				<?php $count++; endforeach; ?>

				<!-- <div id="find-help-box" class="click-toggle click-icon col-md-12 col-xs-12 question-icon n-padding">
					<p class="title goto-bottom c-white p-t-15 f-s-large n-margin">Find Help</p>

					<ul class="menu-toggle display-none n-padding">
						<li class="arrow-right-border-sb">Bankruptcy Forms</li>
						<li class="arrow-right-border-sb">Deputyship Forms</li>
						<li class="arrow-right-border-sb">Simplified Divorce Processing</li>
						<li class="arrow-right-border-sb">View all forms</li>
					</ul>
				</div>
				<div id="donate-box" class="click-toggle click-icon col-md-12 col-xs-12 donate-icon n-padding">
					<p class="title goto-bottom c-white p-t-15 f-s-large n-margin">Donate</p>
					<ul class="menu-toggle display-none n-padding">
						<li class="arrow-right-border-sb">Bankruptcy Forms</li>
						<li class="arrow-right-border-sb">Deputyship Forms</li>
						<li class="arrow-right-border-sb">Simplified Divorce Processing</li>
						<li class="arrow-right-border-sb">View all forms</li>
					</ul>
				</div>
				<div id="volunteer-box" class="click-toggle click-icon col-md-12 col-xs-12 volunteer-icon n-padding">
					<p class="title goto-bottom c-white p-t-15 f-s-large n-margin">Volunteer</p>
					<ul class="menu-toggle display-none n-padding">
						<li class="arrow-right-border-sb">Bankruptcy Forms</li>
						<li class="arrow-right-border-sb">Deputyship Forms</li>
						<li class="arrow-right-border-sb">Simplified Divorce Processing</li>
						<li class="arrow-right-border-sb">View all forms</li>
					</ul>
				</div> -->
			</div>
		</div>
	</div>
	<?php wp_footer(); ?>
</body>
