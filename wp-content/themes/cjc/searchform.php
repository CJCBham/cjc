<form action="<?php echo esc_url(home_url('/')); ?>" method="get" class="menu-search">
    <input type="text" placeholder="Search" name="s" class="footer-search" id="search" value="<?php the_search_query(); ?>">
    <button type="submit" class="search-btn"></button>
</form>