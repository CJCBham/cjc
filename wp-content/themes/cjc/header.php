<?php global $cjcopt; ?>

<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package CJC
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<header class="header">
    <div class="container">
        <nav class="navbar navbar-expand-lg">
            <a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>">
                <img src="<?php echo $cjcopt['cjc-logo']['url'] ?>" width="50" height="44">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <?php
                $menu_name = 'menu-primary';
                $locations = get_nav_menu_locations();
                $menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
                $menuitems = wp_get_nav_menu_items( $menu->term_id, array( 'order' => 'DESC' ) );

                ?>
                <ul class="primary-menu navbar-nav text-md-center nav-justified w-100 align-items-center hide-xs">
                    <?php
                    $count = 0;
                    $counter = 0;
                    $submenu = false;

                    foreach( $menuitems as $item ):

                    $link = $item->url;
                    $title = $item->title;
                    // item does not have a parent so menu_item_parent equals 0 (false)
                    if ( !$item->menu_item_parent ):

                    // save this id for later comparison with sub-menu items
                    $parent_id = $item->ID;
                    ?>

                    <?php if($count==0) { ?>
                    <li class="nav-item nav-home active text-left">
                        <div class="nav-link">
                            <div class="link-item text-center">
                                <a href="<?php echo esc_url(home_url('/')); ?>">
                                    <img src="<?php echo $cjcopt['cjc-logo']['url'] ?>" width="60">
                                </a>
                            </div>
                            <div class="link-item text-left dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                                 aria-expanded="false">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/menu.svg" width="15">
                                Menu
                            </div>

                            <?php } else { ?>

                    <li class="nav-item dropdown">
                        <a href="<?php echo $link; ?>" id="<?php echo "menu-item-".$counter ?>" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?php echo $title; ?>
                        </a>
                        <?php } ?>
                        <?php $counter++; endif; ?>

                        <?php if ( $parent_id == $item->menu_item_parent ): ?>

                            <?php if ( !$submenu ): $submenu = true; ?>
                                <div class="dropdown-menu" aria-labelledby="">
                            <?php endif; ?>

                            <a href="<?php echo $link; ?>" class="dropdown-item"><?php echo $title; ?></a>

                            <?php if($count< (sizeof($menuitems) - 1)) { if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id && $submenu ): ?>
                                </div>
                                <?php $submenu = false; endif; } ?>

                        <?php endif; ?>

                        <?php if($count< (sizeof($menuitems) - 1)) { if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id ): ?>
                        <?php if($count==0) { ?>
            </div>
            <?php } ?>
            </li>
            <?php $submenu = false; endif; } ?>

            <?php $count++; endforeach; ?>
            <li class="nav-item dropdown">
                <ul class="list-unstyled right-menu dropdown-toggle" id="dropdown04" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    <li><img src="<?php echo get_template_directory_uri()?>/images/phone.svg"> <?php echo $cjcopt['cjc-phone-number'] ?> </li>
                    <li><img src="<?php echo get_template_directory_uri()?>/images/email.svg"> <?php echo $cjcopt['cjc-email'] ?> </li>
                </ul>
                <div class="dropdown-menu right-dropdown" aria-labelledby="dropdown04">
                    <div class="dropdown-body">
                        <h4 class="mb-3">Need Help ?</h4>
                        <p>
                            <?php echo $cjcopt['visit-us-editor'] ?>
                        </p>
                        <p><a href="<?php echo $cjcopt['cjc-gmap'] ?>">Google Maps</a></p>
                        <div class="mb-sm social-links">
                            <div class="display-inline-block mr-sm">
                                <a href="<?php echo $cjcopt['cjc-instagram'] ?>" target="_blank"><img src="<?php echo get_template_directory_uri()?>/images/instagram.svg" alt="Instagram"></a>
                            </div>
                            <div class="display-inline-block">
                                <a href="<?php echo $cjcopt['cjc-facebook'] ?>" target="_blank"><img src="<?php echo get_template_directory_uri()?>/images/facebook.svg" alt="Facebook"></a>
                            </div>
                        </div>
                    </div>

                    <form action="<?php echo esc_url(home_url('/')); ?>" method="get"  class="menu-search">
                        <input type="text" placeholder="Search" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Search'" value="<?php the_search_query(); ?>" name="s">
                        <button type="submit" class="search-btn"></button>
                    </form>
                </div>
            </li>
            </ul>

            <ul class="secondary-menu navbar-nav mr-auto show-xs">
                <?php
                $count = 0;
                $counter = 0;
                $submenu = false;

                foreach( $menuitems as $item ):

                $link = $item->url;
                $title = $item->title;
                // item does not have a parent so menu_item_parent equals 0 (false)
                if ( !$item->menu_item_parent ):

                // save this id for later comparison with sub-menu items
                $parent_id = $item->ID;
                ?>

                <li class="nav-item dropdown">
                    <?php
                    if($title !== 'Donate'){
                        ?>
                        <a href="#" id="<?php echo "menu-item-".$counter ?>" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?php echo $title; ?>
                        </a>
                        <?php
                    } else{
                        ?>
                        <a href="<?php echo $link; ?>" id="<?php echo "menu-item-".$counter ?>" class="nav-link dropdown-toggle">
                            <?php echo $title; ?>
                        </a>
                        <?php
                    }
                    ?>
                    <?php $counter++; endif; ?>

                    <?php if ( $parent_id == $item->menu_item_parent ): ?>

                        <?php if ( !$submenu ): $submenu = true; ?>
                            <div class="dropdown-menu" aria-labelledby="">
                        <?php endif; ?>

                        <a href="<?php echo $link; ?>" class="dropdown-item"><?php echo $title; ?></a>

                        <?php if($count< (sizeof($menuitems) - 1)) { if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id && $submenu ): ?>
                            </div>
                            <?php $submenu = false; endif; } ?>

                    <?php endif; ?>

                    <?php if($count< (sizeof($menuitems) - 1)) { if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id ): ?>
                    <?php if($count==0) { ?>
    </div>
    <?php } ?>
    </li>
    <?php $submenu = false; endif; } ?>

    <?php $count++; endforeach; ?>
    </ul>
    </div>
    </nav>
    </div>
</header>

<section class="content">
    <div class="container has-sidebar">
